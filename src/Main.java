import com.zuitt.example.*;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");
        Car myCar = new Car();
        myCar.drive();
        System.out.println(myCar.getDriverName());

        // setters
        myCar.setBrand("Kia");
        myCar.setName("Sorento");
        myCar.setYear_make(2022);
        System.out.println("The "+ myCar.getBrand() + " "+ myCar.getName() +" " + myCar.getYear_make()+ " was driven by " + myCar.getDriverName() );

        // new car detail
        // setters
        myCar.setBrand("Toyota");
        System.out.println("My new car is "+ myCar.getBrand());

        // ANOTHER Car
        Car yourCar = new Car();
        System.out.println("Your car was driven by " + yourCar.getDriverName());

        // ANIMAL and DOG
        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();
        System.out.println(myPet.getName()+ " "+ myPet.getColor()+ " "+ myPet.getBreed());

        Dog yourPet = new Dog();
        yourPet.setName("Oreo");
        System.out.println(yourPet.getBreed()+ " "+ yourPet.getName());

        Person myPerson = new Person();
        myPerson.sleep();
        myPerson.run();
        myPerson.morningGreet();
        myPerson.holidayGreet();

        Computer myComputer = new Computer();
        myComputer.sleep();
        myComputer.run();

        StaticPoly myAddition = new StaticPoly();
        myAddition.addition(5, 4);
        myAddition.addition(5, 4, 5);
        myAddition.addition(18.75, 24.50);

        Child myChild = new Child();
        myChild.speak();

        Parent myParent = new Parent();
        myParent.speak();
        myParent.greet();
        myParent.greet("morning", "Eman");



    }
}

/* JAVA OOP CONCEPTS
Encapsulation = mechanism of wrapping the data (variables) and code acting on the data(methods) together as a single unit.
To achieve encapsulation
    --> declare the variables of the class
    --> provide public setter and getter methods to modify and view the variables.

Composition - modelling objects that are made up of other objects. It defines "has a relationship".
Inheritance - modelling objects that is a subset to another objects. It defines "is a relationship".

AbsTaction - hides away complexities
Interfaces - for multiple inheritance

Polymorphism - many forms
    "poly" = many
    "morph" = forms
    Static or compile-time polymorphism
        -done by function/ method overloading. Overloading can be done through changing number, data type of the arguments
    Dynamic or run-time polymorphism

*/