package com.zuitt.example;

public class Person implements Actions, Greetings {

    public void sleep() {
        System.out.println("Zzzzzzzzz....");
    }

    public void run(){
        System.out.println("Person is running");
    }

    public void morningGreet(){
        System.out.println("Good morning!");
    }

    public void holidayGreet(){
        System.out.println("Happy Holidays!");
    }
}
