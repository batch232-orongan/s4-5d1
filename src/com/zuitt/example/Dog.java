package com.zuitt.example;

public class Dog extends Animal {
    // properties
    // extends - keyword used to inherit the properties of a class.
    private String breed;

    // Constructors
    public Dog(){
        super(); // --> calls the Animal() Constructor
        this.breed = "Golden retriever";
    }

    public Dog(String name, String color, String breed){
        super(name, color); // --> calls the Animal (String name, String color) constructor
        this.breed = breed;
    }

    // getters and setters
    public String getBreed(){
        return this.breed;
    }

    public void setDogBreed(String dogBreed){
        this.breed = breed;
    }

    // methods
    public void speak(){
        super.call(); // --> calls the call() method from aninmal class
        System.out.println("Woof!");
    }




}
