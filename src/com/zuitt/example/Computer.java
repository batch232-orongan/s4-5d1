package com.zuitt.example;

public class Computer implements Actions{

    public void sleep() {
        System.out.println("Computer is sleeping");
    }

    public void run(){
        System.out.println("Computer is running");
    }
}
