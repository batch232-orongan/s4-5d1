package com.zuitt.example;

public class Parent {
    // mini activity
    // static Polymorphism with greet method
        // first method - empty parameter, print message saying "Hello friend"
        // second method - message will be "Good " +timeOfTheDay + name

    public void greet(){
        System.out.println("Hello Friend!");
    }

    public String greet(String timeOfTheDay, String name){
        System.out.println("Good " + timeOfTheDay + " "+ name);
        return timeOfTheDay + name;
    }
    /*
 Miss Tines Solution
    public void greet(String timeOfTheDay, String name){
        System.out.println("Good " + timeOfTheDay + " "+ name);
    }
*/

    public void speak(){
        System.out.println("I am preparing");
    }


}
