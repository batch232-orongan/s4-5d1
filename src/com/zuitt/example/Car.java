package com.zuitt.example;

public class Car {
    // Object Oriented Concepts
    // Object - abstract idea that represents something in the real world.
    // Class - representation  of object using code.
    // Instance - unique copy of the idea, made "physical".

    // Objects
        // States and Attributes - what is the idea about?
        // Behaviors - what can idea do?
    // Example: a person has attributes like name, age, height and weight. And a person can eat, sleep and speak.

    // 4 PARTS OF THE CLASS
    // Properties - characteristics of the object
        // private: accessible only within the class
        private String name;
        private String brand;
        private int year_make;
        private Driver d;


    // Constructors - used to create an object
        // 2 TYPES OF CONSTRUCTOR
        // parameterized constructor
        public Car(String name, String brand, int year_make){
            this.name = name;
            this.brand = brand;
            this.year_make= year_make;
            this.d = new Driver("Alejandro"); // for every car created, there will be a driver named Alejandro
        }
        // public constructors
        public Car(){
            this.d = new Driver("Alejandro");
        };

    // Getters and Setters - get and set the value of each property of the object
        // getters - retrieving the value of the instantiated object.
        public String getName(){
            return this.name;
        }
        public String getBrand(){
            return this.brand;
        }
        public int getYear_make(){
            return this.year_make;
        }
        // setters - sets the values.
        public void setName(String name){
            this.name = name;
        }
        public void setBrand(String brand){
            this.brand = brand;
        }
        public void setYear_make(int year_make){
            this.year_make = year_make;
        }

    // Methods - functions on object can perform.
        public void drive(){
            System.out.println("The car goes vroom vroom!");
        }
        public String getDriverName(){
            return this.d.getName();
        }




}

