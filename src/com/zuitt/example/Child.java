package com.zuitt.example;

public class Child extends Parent{
    // constructor
    public Child(){
        super();
    }

    // methods
    public void speak(){
        System.out.println("I am a mere child");
    }

}
