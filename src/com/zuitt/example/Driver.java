package com.zuitt.example;

public class Driver {
    private String name;

    public Driver(String name){
        this.name = name;
    }
    public Driver (){};

    public String getName(){
        return this.name;
    }

    public void setDriverName(String name){
        this.name = name;
    }
}
