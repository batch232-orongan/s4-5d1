package com.zuitt.example;

public class StaticPoly {
    public int addition(int a, int b){
        System.out.println(a + b);
        return a+ b;
    }
    // overload by changing the number of arguments
    public int addition(int a, int b, int c){
        System.out.println(a+ b+ c);
        return a + b+ c;
    }
    // overload by changing the data type of arguments
    public double addition (double a, double b){
        System.out.println(a + b);
        return a + b;
    }



}
